const app = require('./app');

const port = process.env.SERVER_PORT || 80;
app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`);
});
