const gulp = require('gulp');
const less = require('gulp-less');
const babel = require('babelify');
const browserify = require('browserify');
const source = require('vinyl-source-stream');

gulp.task('css', function () {

    let stream = gulp.src('./frontend/css/main.less')
       .pipe(less())
       .pipe(gulp.dest('./public/css'));
});

gulp.task('js', function() {
    var b = browserify({
        entries: ['./frontend/js/main.js']
    }).transform(babel, {
        presets: ['es2015', 'react']
    });

    let stream = b.bundle()
        .pipe(source('main.js'))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('watcher', function() {
    gulp.watch('frontend/css/**/*.less', ['css']);
    gulp.watch('frontend/js/**/*.js', ['js']);
});

gulp.task('build', ['css', 'js']);
gulp.task('dev', ['css', 'js', 'watcher']);
