const express = require('express');
const app = express();

module.exports = app;

app.use(express.static('public'));

app.get('/api', function (req, res) {
    setTimeout(() => {
        res.send({
            message: 'Hello World!'
        });
    }, 1000);
});
