import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import App from '../frontend/js/components/App';

describe('<App/>', function() {
    it('should have an <h1>', function() {
        const wrapper = shallow(<App / >);
        expect(wrapper.find('h1')).to.have.length(1);
    });

    it('should display loading message', function() {
        const wrapper = shallow(<App / >);
        expect(wrapper.text()).to.contain('Loading');
    });

    it('should display the text of state property', function() {
        const wrapper = shallow(<App / >);
        wrapper.setState({ message: 'Hello' });
        expect(wrapper.text()).to.equal('Hello');
    });
});
