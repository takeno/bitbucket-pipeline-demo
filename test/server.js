const request = require('supertest');
const app = require('../app');


describe('GET /', function() {
    it('respond with json', function(done) {
        request(app)
            .get('/api')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
    it('response contains "Hello World"', function(done) {
        request(app)
            .get('/api')
            .expect(200, {
                message: 'Hello World!',
            }, done);
    });
});
