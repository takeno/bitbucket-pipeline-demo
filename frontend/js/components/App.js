import React, {Component} from 'react';
import request from 'superagent';


export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            message: 'Loading...',
        };
    }

    componentDidMount() {
        request
          .get('/api')
          .set('Accept', 'application/json')
          .end((err, res) => {
              this.setState({
                  message: res.body.message
              });
          });
    }
    render() {
        return <h1>{this.state.message}</h1>;
    }
}
